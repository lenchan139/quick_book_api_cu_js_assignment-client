var express = require('express');
var request = require('request')
var router = express.Router();
const baseUrl = "https://quick-book-cu-assignment.herokuapp.com"
/* GET home page. */
router.get('/', function(req, res, next) {
  let url = baseUrl + "/book/"
  request.get({
    url: url,
    json: true,
    headers: {'User-Agent': 'request'}
  }, (err, res1, data) => {
    if (err) {
      console.log('Error:', err);
      next(err)
    } else if (res.statusCode !== 200) {
      console.log('Status:', res.statusCode);
      res.send(res1)
    } else {
      // data is already parsed as JSON:
      console.log(data);
      res.render('index', {session:req.session, json: data });
    }
  })
});

router.get('/book/:id', function(req, res, next) {
  let id = req.params.id
  let url = baseUrl + "/book/" + id
  request.get({
    url: url,
    json: true,
    headers: {'User-Agent': 'request'}
  }, (err, res1, data) => {
    if (err) {
      console.log('Error:', err);
      next(err)
    } else if (res.statusCode !== 200) {
      console.log('Status:', res.statusCode);
      res.send(res1)
    } else {
      // data is already parsed as JSON:
      console.log(data);
      let cmurl = baseUrl + "/comment/book/" + id
      request.get({
        url: cmurl,
        json: true,
        headers: {'User-Agent': 'request'}
      }, (err1, res11, data1) => {
        if (err) {
          console.log('Error:', err1);
          next(err1)
        }  else {
          console.log(data1)
          res.render('bookDetails', {session : req.session, json : data, cm:data1});
        }
      })
    }
  })
});
router.get('/newbook', function(req,res,next){
  if(req.session.username){
    res.render('newbook',{session:req.session})
  }else{
    res.send('<script>window.onload = function() {alert("Please Login first!");window.location.replace("/");};</script>')

  }
})
router.post('/newbook',function(req,res,next){
  console.log(req.body)
  if(req.session.username){
    let auth = "Basic " + new Buffer(req.session.username + ":" + req.session.password).toString("base64")
    let url = baseUrl + "/book/"
    console.log("URL: " + url)
    request.post({
      url: url,
      json: true,
      form :req.body,
      headers: {'User-Agent': 'request',Authorization:auth}
    }, (err, res1, data) => {
      if (err) {
        console.log('Error:', err);
        next(err)
      } else if (res.statusCode !== 200) {
        console.log('Status:', res.statusCode);
        res.send(data)
      } else {
        //here
        console.log(data)
        res.send('<script>window.onload = function() {alert("Action Complete!");window.location.replace("/");};</script>')

      }
    })
  }else{
    res.send('<script>window.onload = function() {alert("Please Login first!");window.location.replace("/");};</script>')

  }
})
router.get('/editbook/:id', function(req,res,next){
  let id = req.params.id
  let url = baseUrl + "/book/" + id + "/"
  if(req.session.username && id){
    request.get({
      url: url,
      json: true,
      headers: {'User-Agent': 'request'}
    }, (err, res1, data) => {
      if (err) {
        console.log('Error:', err);
        next(err)
      } else if (res.statusCode !== 200) {
        console.log('Status:', res.statusCode);
        res.send(data)
      } else {
        //here
    res.render('editbook',{session:req.session,json:data})
  }
})
  }else{
    res.send('<script>window.onload = function() {alert("Please Login first!");window.location.replace("/");};</script>')

  }
})
router.post('/editbook/:id',function(req,res,next){
  console.log(req.body)
  if(req.session.username){
    let auth = "Basic " + new Buffer(req.session.username + ":" + req.session.password).toString("base64")
    let url = baseUrl + "/book/" + id + "/"
    console.log("URL: " + url)
    request.put({
      url: url,
      json: true,
      form :req.body,
      headers: {'User-Agent': 'request',Authorization:auth}
    }, (err, res1, data) => {
      if (err) {
        console.log('Error:', err);
        next(err)
      } else if (res.statusCode !== 200) {
        console.log('Status:', res.statusCode);
        res.send(data)
      } else {
        //here
        console.log(data)
        res.send('<script>window.onload = function() {alert("Action Complete!");window.location.replace("/");};</script>')

      }
    })
  }else{
    res.send('<script>window.onload = function() {alert("Please Login first!");window.location.replace("/");};</script>')

  }
})

router.get('/delbook/:id', function(req,res,next){
  let id = req.params.id
  if(id && req.session.username){
    res.render('delbook', {id:id})
  }else{
    res.send('<script>window.onload = function() {alert("Please Login first!");window.location.replace("/");};</script>')

  }
})
router.post('/delbook/:id', function(req,res,next){
  let id = req.params.id
  if(id && req.session.username){
    let auth = "Basic " + new Buffer(req.session.username + ":" + req.session.password).toString("base64")
    let url = baseUrl + "/book/" + id + "/"
    console.log("URL: " + url)
    request.delete({
      url: url,
      json: true,
      form :req.body,
      headers: {'User-Agent': 'request',Authorization:auth}
    }, (err, res1, data) => {
      if (err) {
        console.log('Error:', err);
        next(err)
      } else if (res.statusCode !== 200) {
        console.log('Status:', res.statusCode);
        res.send(data)
      } else {
        res.send('<script>window.onload = function() {alert("Action Complete!");window.location.replace("/");};</script>')

      }
    })
  }
})
router.post('/newComment/:id',function(req,res,next){
    console.log(req.body)
    let id = req.params.id
    let url = baseUrl + "/comment/"
    request.post({
      url: url,
      json: true,
      form :req.body,
      headers: {'User-Agent': 'request'}
    }, (err, res1, data) => {
      if (err) {
        console.log('Error:', err);
        next(err)
      } else if (res.statusCode !== 200) {
        console.log('Status:', res.statusCode);
        res.send(data)
      } else {
        //here
        console.log(data)
        res.send('<script>window.onload = function() {alert("Action Complete!");window.location.replace("/book/' + id + '");};</script>')

      }
    })

})

router.get('/delComment/:id', function(req,res,next){
  let id = req.params.id
  if(id && req.session.username){
    let auth = "Basic " + new Buffer(req.session.username + ":" + req.session.password).toString("base64")
    let url = baseUrl + "/comment/" + id + "/"
    console.log("URL: " + url)
    request.delete({
      url: url,
      json: true,
      form :req.body,
      headers: {'User-Agent': 'request',Authorization:auth}
    }, (err, res1, data) => {
      if (err) {
        console.log('Error:', err);
        next(err)
      } else if (res.statusCode !== 200) {
        console.log('Status:', res.statusCode);
        res.send(data)
      } else {
        res.send('<script>window.onload = function() {alert("Action Complete!");window.location.replace("/");};</script>')

      }
    })
  }
})
module.exports = router;
