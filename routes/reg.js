var express = require('express');
var request = require('request')
var router = express.Router();
const baseUrl = "https://quick-book-cu-assignment.herokuapp.com"
/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('reg',{})
})
router.post('/',function(req,res,next){
  var username = req.body.username
  let password = req.body.password
  let email = req.body.email
  let nickname = req.body.nickname
  let group = "maintainer"
  console.log(req.body)
  if(username && password && email && nickname && group){
    let url = baseUrl + "/user/"
    request.post({
      url: url,
      json: true,
      form : {
        username: username,
        password: password,
        email: email,
        nickname: nickname,
        group: group
      },
      headers: {'User-Agent': 'request'}
    }, (err, res1, data) => {
      if (err) {
        console.log('Error:', err);
        next(err)
      } else if (res.statusCode !== 200) {
        console.log('Status:', res.statusCode);
        res.send(res1)
      } else {
        console.log(data)
        if(username == data.username){
          res.send('<script>window.onload = function() {alert("Login success!");window.location.replace("/");};</script>')
        }
      }
    })
  }
})
module.exports = router;
