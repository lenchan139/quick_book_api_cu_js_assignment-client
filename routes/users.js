var express = require('express');
var router = express.Router();

var request = require('request')

const baseUrl = "https://quick-book-cu-assignment.herokuapp.com"
/* GET users listing. */
router.get('/', function(req, res, next) {
  res.render('login',{})
});

router.post('/', function(req, res, next){
  let username = req.body.username
  let password = req.body.password
  let auth = "Basic " + new Buffer(username + ":" + password).toString("base64")
  if(username && password ){
    let url = baseUrl + "/user/username/" + username
    request.get({
      url: url,
      json: true,
      headers: {'User-Agent': 'request',
                'Authorization': auth}
    }, (err, res1, data) => {
      console.log(data)
      if(err){
        next(err)
      }else if(!data){
        res.redirect('/users/?msg=errPw')
      }else{
        if(username == data.username){

          req.session.username = username
          req.session.password = password
          req.session.nickname = data.nickname
          res.send('<script>window.onload = function() {alert("Login success!");window.location.replace("/");};</script>')
        }else{
          res.redirect('/users/?msg=errPw')
        }
      }
    })
  }else{
    res.send('<script>window.onload = function() {alert("username and password incorrent!");window.location.replace("/");};</script>')

  }
})
router.get('/logout',function(req, res, next){
  req.session.destroy(function(err){
    if(err){
      next(err)
    }else{
      res.send('<script>window.onload = function() {alert("Logout success!");window.location.replace("/");};</script>')
    }
  })
})
module.exports = router;
